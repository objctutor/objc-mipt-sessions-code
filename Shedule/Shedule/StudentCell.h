//
//  StudentCell.h
//  Shedule
//
//  Created by Artemiy Sobolev on 12/12/13.
//  Copyright (c) 2013 Artemiy Sobolev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StudentCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *button;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UILabel *label;

@end
