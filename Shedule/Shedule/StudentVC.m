//
//  SHViewController.m
//  Shedule
//
//  Created by Artemiy Sobolev on 03/10/13.
//  Copyright (c) 2013 Artemiy Sobolev. All rights reserved.
//

#import "StudentVC.h"
#import "SHStudent.h"
#import "SHGroup.h"


@interface StudentVC ()
@property (strong, nonatomic) IBOutlet UITextField *firstName;
@property (strong, nonatomic) IBOutlet UITextField *secondName;

@property (nonatomic, strong) SHStudent *student;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *saveButton;

@end

@implementation StudentVC


- (void)viewDidLoad{
    [super viewDidLoad];
    
    if (self.student) {
        self.firstName.text = self.student.firstName;
        self.secondName.text = self.student.secondName;

    }
    else {
        self.firstName.text = @"";
        self.secondName.text = @"";
    }
}

- (BOOL)shouldEnableSaveButton{
    if ([self.firstName.text isEqualToString:@""])
        return NO;
    if ([self.secondName.text isEqualToString:@""])
        return NO;

    
    if ([self.firstName.text isEqualToString:self.student.firstName] && [self.secondName.text isEqualToString:self.student.secondName]) {
        return NO;
    }
    
    return YES;
}

- (IBAction)nameWasEntered:(id)sender {
    [self.saveButton setEnabled:[self shouldEnableSaveButton]];
}

- (IBAction)surnameWasEntered:(id)sender {
    [self.saveButton setEnabled:[self shouldEnableSaveButton]];
}

- (IBAction)saveButtonPressed:(id)sender {
    SHStudent *newStudent = [[SHStudent alloc] init];
    newStudent.firstName = self.firstName.text;
    newStudent.secondName = self.secondName.text;
    
    if (self.student)
        [_delegate changeStudentTo:newStudent];
    else
        [_delegate addNewStudent:newStudent];
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}


@end
