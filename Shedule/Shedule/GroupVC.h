//
//  GroupVC.h
//  Shedule
//
//  Created by Artemiy Sobolev on 01/12/13.
//  Copyright (c) 2013 Artemiy Sobolev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StudentVC.h"

@interface GroupVC : UITableViewController <StudentVCDelegate>

@end
