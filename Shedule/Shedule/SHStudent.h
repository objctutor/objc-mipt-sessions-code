//
//  SHStudent.h
//  Shedule
//
//  Created by Artemiy Sobolev on 03/10/13.
//  Copyright (c) 2013 Artemiy Sobolev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SHStudent : NSObject

@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *secondName;
@property (nonatomic, strong) NSNumber *groupNumber;

- (NSString *)fullName;

- (NSDictionary *)plist;
- (id)initWithPlist:(NSDictionary *)plist;

@end
