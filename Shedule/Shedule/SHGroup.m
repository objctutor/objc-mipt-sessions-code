//
//  SHGroup.m
//  Shedule
//
//  Created by Artemiy Sobolev on 10/10/13.
//  Copyright (c) 2013 Artemiy Sobolev. All rights reserved.
//

#import "SHGroup.h"
#define kGroupNumber @"groupNumber"
#define kStudents @"students"


@interface SHGroup()
@property (nonatomic, strong) NSNumber *groupNumber;
@property (nonatomic, strong) NSMutableOrderedSet *studentsSet;
@property (nonatomic) BOOL readingWasFromiCloud;
@end
@implementation SHGroup

- (id)initWithGroupNumber:(NSNumber *)groupNumber{
    self = [super init];
    if (self) {
        _groupNumber = groupNumber;

        NSFileCoordinator *fileCoordinator = [[NSFileCoordinator alloc] initWithFilePresenter:nil];
        NSURL *iCloudURL = [[self iCloudDocs] URLByAppendingPathComponent:[[self localURL] lastPathComponent]];
        
        [fileCoordinator coordinateReadingItemAtURL:iCloudURL
                                            options:NSFileCoordinatorReadingResolvesSymbolicLink
                                              error:nil
                                         byAccessor:^(NSURL *URLToUse) {
                                             NSDictionary *dict = [NSDictionary dictionaryWithContentsOfURL:URLToUse];
                                             if (dict) self.readingWasFromiCloud = YES;

                                             NSMutableOrderedSet *set = [[NSMutableOrderedSet alloc] init];
                                             for (NSDictionary *studentDict in dict[kStudents]) {
                                                 [set addObject:[[SHStudent alloc] initWithPlist:studentDict]];
                                             }
                                             self.studentsSet = set;
                                         }];
    }
    return self;
}

- (NSDictionary *)plist{
    if (!self.groupNumber) return nil;
    NSMutableArray *studentsToSave = [[NSMutableArray alloc] init];
    
    
    for (SHStudent *student in self.studentsSet)
        [studentsToSave addObject:[student plist]];
    
    return @{kGroupNumber: self.groupNumber, kStudents : studentsToSave};
}

- (id)initWithPlist:(NSDictionary *)plist{
    self = [super init];
    if (self) {
        _groupNumber = plist[kGroupNumber];
        NSArray *studentsFromDisk = plist[kStudents];
        
        for (NSDictionary *studentInDict in studentsFromDisk)
            [self.studentsSet addObject:[[SHStudent alloc] initWithPlist:studentInDict]];
    }
    return self;
}

- (NSURL *)iCloudDocs{
    NSURL *iCloudContUrl  = [[NSFileManager defaultManager] URLForUbiquityContainerIdentifier:nil];
    
    return [iCloudContUrl URLByAppendingPathComponent:@"Documents"];
}

- (NSURL *)localURL{
    NSString *lastPath = [NSString stringWithFormat:@"%@.plist", self.groupNumber];
    NSURL *localURL = [NSURL fileURLWithPath:[self rootPath]];
    
    return [localURL URLByAppendingPathComponent:lastPath];
}

- (void)save{
//
//    
    NSURL *localURL = [self localURL];
    NSString *name = [localURL lastPathComponent];
    NSURL *iCloudURL = [[self iCloudDocs] URLByAppendingPathComponent:name];
    
//    [[self plist] writeToFile:[self fullPath] atomically:YES];
//
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        NSError *error;
//        [[[NSFileManager alloc] init] setUbiquitous:YES itemAtURL:localURL destinationURL:iCloudURL error:&error];
//        if (error) NSLog(@"the error is %@", error);
//    });
//
    NSURL *urlToWriteTo = self.readingWasFromiCloud ? iCloudURL : localURL;
    NSFileCoordinator *fileCoordinator = [[NSFileCoordinator alloc] initWithFilePresenter:nil];
    [fileCoordinator coordinateWritingItemAtURL:urlToWriteTo  options:NSFileCoordinatorWritingForReplacing error:nil byAccessor:^(NSURL *newURL) {
        [[self plist] writeToURL:newURL atomically:YES];
        if (!self.readingWasFromiCloud) [[[NSFileManager alloc] init] setUbiquitous:YES itemAtURL:localURL destinationURL:iCloudURL error:nil];
    }];
    
}

- (void)addStudent:(SHStudent *)newStudent{
    newStudent.groupNumber = self.groupNumber;
    [self.studentsSet addObject:newStudent];
    [self save];
}

- (void)changeStudentAtIndex:(NSInteger)index toNewOne:(SHStudent *)newStudent{
    if (index < 0 || index >= self.studentsSet.count) {
        NSLog(@"bubuh error");
        return;
    }
    [self.studentsSet setObject:newStudent atIndex:index];
    [self save];
}

- (void)removeStudentAtIndex:(NSInteger)index{
    if (index < 0 || index >= self.studentsSet.count) {
        NSLog(@"bubuh error");
        return;
    }
    [self.studentsSet removeObjectAtIndex:index];
    [self save];
}


- (NSString *)fullPath{
    return [NSString stringWithFormat:@"%@/%@.plist", [self rootPath], self.groupNumber];
}

- (NSString *)rootPath{
    return NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
}
- (NSOrderedSet *)students{
    return self.studentsSet;
}

- (NSMutableOrderedSet *)studentsSet{
    if (!_studentsSet) {
        _studentsSet = [[NSMutableOrderedSet alloc] init];
    }
    return _studentsSet;
}



@end
