//
//  GroupVC.m
//  Shedule
//
//  Created by Artemiy Sobolev on 01/12/13.
//  Copyright (c) 2013 Artemiy Sobolev. All rights reserved.
//

#import "GroupVC.h"
#import "SHGroup.h"
#import "SHStudent.h"
#import "StudentVC.h"

#import "StudentCell.h"


@interface GroupVC ()
@property (nonatomic, strong) SHGroup *group;
@property (nonatomic, strong) NSMetadataQuery *iCloudQuery;

@end

@implementation GroupVC

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
}

- (void)awakeFromNib{
    if (![self.iCloudQuery isStarted]) [self.iCloudQuery startQuery];
    [self.iCloudQuery enableUpdates];
}

- (IBAction)addButtonPressed:(id)sender {
    [self performSegueWithIdentifier:@"toAddNewStudentSegID" sender:self];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.group.students.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"StudentCellRID";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    SHStudent *student = self.group.students[indexPath.row];

    
    [(StudentCell *)cell label].text =[student fullName];
    [(StudentCell *)cell imageView].image = [UIImage imageNamed:@"student"];
    
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self performSegueWithIdentifier:@"toStudentVCSegID" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier] isEqualToString:@"toStudentVCSegID"]) {
        StudentVC *destVC = [segue destinationViewController];
        NSInteger index = self.tableView.indexPathForSelectedRow.row;
        SHStudent *student = self.group.students[index];
        [destVC setStudent:student];
        [destVC setDelegate:self];
    }
    else if ([[segue identifier] isEqualToString:@"toAddNewStudentSegID"]) {
        StudentVC *destVC = [segue destinationViewController];
        [destVC setStudent:nil];
        [destVC setDelegate:self];
    }
}


- (SHGroup *)group{
    if (!_group) {
        _group = [[SHGroup alloc] initWithGroupNumber:@(211)];
    }
    return _group;
}


- (void)addNewStudent:(SHStudent *)newStudent{
    newStudent.groupNumber = self.group.groupNumber;
    [self.group addStudent:newStudent];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

- (void)changeStudentTo:(SHStudent *)student{
    student.groupNumber = self.group.groupNumber;

    NSInteger index = self.tableView.indexPathForSelectedRow.row;
    [self.group changeStudentAtIndex:index toNewOne:student];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.group removeStudentAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];

    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}

- (void)processCloudQueryResults:(NSNotification *)notification{
    NSLog(@"processing icloud results");
    NSMutableDictionary *urlsFromiCloud = [[NSMutableDictionary alloc] initWithCapacity:self.iCloudQuery.resultCount];
    [self.iCloudQuery disableUpdates];
    for (int i = 0; i < self.iCloudQuery.resultCount; i++) {
        NSMetadataItem *item = [self.iCloudQuery resultAtIndex:i];
        NSURL *url = [item valueForAttribute:NSMetadataItemURLKey];
        [urlsFromiCloud setObject:url forKey:[url lastPathComponent]];
    }
    
    [self.iCloudQuery enableUpdates];
    self.group = nil;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
    
}

- (NSMetadataQuery *)iCloudQuery{
    if (!_iCloudQuery) {
        _iCloudQuery = [[NSMetadataQuery alloc] init];
        _iCloudQuery.searchScopes = [NSArray arrayWithObject:NSMetadataQueryUbiquitousDocumentsScope];
        _iCloudQuery.predicate = [NSPredicate predicateWithFormat:@"%K like '*'", NSMetadataItemFSNameKey];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(processCloudQueryResults:) name:NSMetadataQueryDidFinishGatheringNotification object:_iCloudQuery];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(processCloudQueryResults:) name:NSMetadataQueryDidUpdateNotification object:_iCloudQuery];
    }
    return _iCloudQuery;
}



@end
