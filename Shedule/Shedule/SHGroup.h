//
//  SHGroup.h
//  Shedule
//
//  Created by Artemiy Sobolev on 10/10/13.
//  Copyright (c) 2013 Artemiy Sobolev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SHStudent.h"

@interface SHGroup : NSObject

- (id)initWithGroupNumber:(NSNumber *)groupNumber;

- (void)addStudent:(SHStudent *)newStudent;

- (void)changeStudentAtIndex:(NSInteger)index toNewOne:(SHStudent *)newStudent;
- (void)removeStudentAtIndex:(NSInteger)index;



- (NSOrderedSet *)students;
- (NSNumber *)groupNumber;

@end
