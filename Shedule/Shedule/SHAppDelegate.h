//
//  SHAppDelegate.h
//  Shedule
//
//  Created by Artemiy Sobolev on 03/10/13.
//  Copyright (c) 2013 Artemiy Sobolev. All rights reserved.
// test

#import <UIKit/UIKit.h>

@interface SHAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
