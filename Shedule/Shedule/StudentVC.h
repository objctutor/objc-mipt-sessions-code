//
//  SHViewController.h
//  Shedule
//
//  Created by Artemiy Sobolev on 03/10/13.
//  Copyright (c) 2013 Artemiy Sobolev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SHStudent.h"

@protocol StudentVCDelegate
- (void)addNewStudent:(SHStudent *)newStudent;
- (void)changeStudentTo:(SHStudent *)student;

@end

@interface StudentVC : UIViewController

- (void)setStudent:(SHStudent *)student;

@property (weak) id delegate;

@end
