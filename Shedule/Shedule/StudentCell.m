//
//  StudentCell.m
//  Shedule
//
//  Created by Artemiy Sobolev on 12/12/13.
//  Copyright (c) 2013 Artemiy Sobolev. All rights reserved.
//

#import "StudentCell.h"

@implementation StudentCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)buttonPressed:(id)sender {
    NSLog(@"my name is %@", self.label.text);
}

@end
