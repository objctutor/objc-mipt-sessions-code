//
//  SHStudent.m
//  Shedule
//
//  Created by Artemiy Sobolev on 03/10/13.
//  Copyright (c) 2013 Artemiy Sobolev. All rights reserved.
//

#import "SHStudent.h"
@interface SHStudent()
@property (nonatomic, strong) NSNumber *courseNumber;
@end

@implementation SHStudent

- (id)init{
    self = [super init];
    if (self) {
        
        
        
        
    }
    return self;
}

- (id)initWithFirstName:(NSString *)firstName andSecondName:(NSString *)secondName andGroupNumber:(NSString *)groupNumber{
    self = [super init];
    if (self) {
        _firstName = firstName;
        _secondName = secondName;
        self.groupNumber = groupNumber;
    }
    return self;
}

- (void)skipAYear:(NSString *)newGroupNumber{
    self.groupNumber = newGroupNumber;
}

- (void)setGroupNumber:(NSString *)groupNumber{
    _groupNumber = groupNumber;
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    _courseNumber = [formatter numberFromString:groupNumber];
}

- (NSNumber *)courseNumber{
    if (!_courseNumber)
        _courseNumber = @111;
    return _courseNumber;
}

@end
