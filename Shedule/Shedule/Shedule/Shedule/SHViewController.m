//
//  SHViewController.m
//  Shedule
//
//  Created by Artemiy Sobolev on 03/10/13.
//  Copyright (c) 2013 Artemiy Sobolev. All rights reserved.
//

#import "SHViewController.h"
#import "SHStudent.h"


@interface SHViewController ()
@property (strong, nonatomic) IBOutlet UITextField *groupNumberTF;
@property (strong, nonatomic) IBOutlet UITextField *fName;
@property (strong, nonatomic) IBOutlet UITextField *sName;
@property (strong, nonatomic) IBOutlet UITextField *counteTF;

@property (nonatomic, strong) SHStudent *artemiy;
@property (nonatomic) int counter;
@end

@implementation SHViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    
}

- (SHStudent *)artemiy{
    if (!_artemiy) {
        _artemiy = [[SHStudent alloc] initWithFirstName:@"artemiy" andSecondName:@"sobolev" andGroupNumber:@"773"];
    }
    return _artemiy;
}

- (void)setCounter:(int)counter{
    if (counter > 100) {
        _counter = 0;
        return;
    }
    
    _counter = counter;
    self.counteTF.text = [NSString stringWithFormat:@"count number is: %d", counter];
}

- (IBAction)buttonPressed:(id)sender {
    self.counter++;
    if (self.counter == 2){
        [self.artemiy skipAYear:@"877"];
    }
    self.groupNumberTF.text = self.artemiy.groupNumber;
    self.fName.text = self.artemiy.firstName;
    self.sName.text = self.artemiy.secondName;
    
    
}


@end
