//
//  SHStudent.h
//  Shedule
//
//  Created by Artemiy Sobolev on 03/10/13.
//  Copyright (c) 2013 Artemiy Sobolev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SHStudent : NSObject

@property (nonatomic, strong) NSString *groupNumber;

@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *secondName;

- (id)init;
- (void)skipAYear:(NSString *)newGroupNumber;
- (NSNumber *)courseNumber;


- (id)initWithFirstName:(NSString *)firstName andSecondName:(NSString *)secondName andGroupNumber:(NSString *)groupNumber;

@end
