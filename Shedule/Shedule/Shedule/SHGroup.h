//
//  SHGroup.h
//  Shedule
//
//  Created by Artemiy Sobolev on 10/10/13.
//  Copyright (c) 2013 Artemiy Sobolev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SHStudent.h"

@interface SHGroup : NSObject

- (id)initWithNumber:(NSNumber *)number;

- (void)addStudent:(SHStudent *)newStudent;


@end
