//
//  SHViewController.m
//  Shedule
//
//  Created by Artemiy Sobolev on 03/10/13.
//  Copyright (c) 2013 Artemiy Sobolev. All rights reserved.
//

#import "SHViewController.h"
#import "SHStudent.h"
#import "SHGroup.h"


@interface SHViewController ()
@property (strong, nonatomic) IBOutlet UITextField *groupNumberTF;
@property (strong, nonatomic) IBOutlet UITextField *fName;
@property (strong, nonatomic) IBOutlet UITextField *sName;

@property (nonatomic, strong) SHStudent *student;
@property (nonatomic, strong) SHGroup *group;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *saveButton;
@property (strong, nonatomic) IBOutlet UIImageView *studentImageView;


@end

@implementation SHViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    self.studentImageView.image = [UIImage imageNamed:@"student"];
}

- (IBAction)nameWasEntered:(id)sender {
//  from IBAction we know who is the sender
//  these code generates everytime value is changed
    self.student.firstName = [sender text];
    if ([self.student canSave]) [self.saveButton setEnabled:YES];
}

- (IBAction)surnameWasEntered:(id)sender {
//  from IBAction we know who is the sender
//  these code generates everytime value is changed
    self.student.secondName = [sender text];
    if ([self.student canSave]) [self.saveButton setEnabled:YES];
}

- (IBAction)groupNameWasEntered:(id)sender {
//  from IBAction we know who is the sender
//  these code generates everytime value is changed
    self.student.groupNumber = [sender text];
    NSLog(@"%@", [sender text]);
    if ([self.student canSave]) [self.saveButton setEnabled:YES];
}

- (SHStudent *)student {
    if (!_student)
        _student = [[SHStudent alloc] init];
    return _student;
}

- (IBAction)buttonPressed:(id)sender {
    [self.student saveHim];
}


@end
