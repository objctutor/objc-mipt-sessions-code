//
//  SHGroup.m
//  Shedule
//
//  Created by Artemiy Sobolev on 10/10/13.
//  Copyright (c) 2013 Artemiy Sobolev. All rights reserved.
//

#import "SHGroup.h"
@interface SHGroup()
@property (nonatomic, strong) NSNumber *numberOfGroup;
@property (nonatomic, strong) NSMutableArray *students;
@end
@implementation SHGroup

- (id)initWithNumber:(NSNumber *)number{
    self = [super init];
    if (self) {
        _numberOfGroup = number;
    }
    return self;
}

- (void)addStudent:(SHStudent *)newStudent{
    [self.students addObject:newStudent];
    NSString *fullPath = [NSString stringWithFormat:@"%@/%@.plist", [self rootPath], self.numberOfGroup];
    [self.students writeToFile:fullPath atomically:YES];
}

- (NSString *)rootPath{
    return NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
}

- (NSMutableArray *)students{
    if (!_students) {
        _students = [[NSMutableArray alloc] init];
    }
    return _students;
}

@end
