//
//  SHStudent.m
//  Shedule
//
//  Created by Artemiy Sobolev on 03/10/13.
//  Copyright (c) 2013 Artemiy Sobolev. All rights reserved.
//

#define kName @"name"
#define kSurname @"surname"
#define kGroupNumber @"groupNumber"

#import "SHStudent.h"
@interface SHStudent()
@property (nonatomic, strong) NSNumber *courseNumber;
@end

@implementation SHStudent

- (id)init{
    self = [super init];
    if (self) {
        
        
        
        
    }
    return self;
}

- (BOOL)canSave{
    if ((!self.firstName)||(!self.secondName)||(!self.groupNumber))
        return NO;
    return YES;
}


- (BOOL)saveHim{
    if (![self canSave]) return NO;
    
    NSDictionary *studentsDict = @{kName:self.firstName, kSurname : self.secondName, kGroupNumber : self.groupNumber};
    NSString *fullPath = [NSString stringWithFormat:@"%@/%@.plist", [self rootPath], self.groupNumber];
    
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:fullPath];
    BOOL result = NO;
    
    if (fileExists) {
        NSMutableArray *groupOfStudents = [[NSMutableArray alloc] initWithContentsOfFile:fullPath];
        [groupOfStudents addObject:studentsDict];
        result = [groupOfStudents writeToFile:fullPath atomically:YES];
    }
    else
        result = [@[studentsDict] writeToFile:fullPath atomically:YES];
    return result;
}

- (NSString *)rootPath{
    return NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
}

- (id)initWithFirstName:(NSString *)firstName andSecondName:(NSString *)secondName andGroupNumber:(NSString *)groupNumber{
    self = [super init];
    if (self) {
        _firstName = firstName;
        _secondName = secondName;
        self.groupNumber = groupNumber;
    }
    return self;
}

- (void)skipAYear:(NSString *)newGroupNumber{
    self.groupNumber = newGroupNumber;
}

- (void)setGroupNumber:(NSString *)groupNumber{
    _groupNumber = groupNumber;
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    _courseNumber = [formatter numberFromString:groupNumber];
}

- (NSNumber *)courseNumber{
    if (!_courseNumber)
        _courseNumber = @111;
    return _courseNumber;
}

@end
