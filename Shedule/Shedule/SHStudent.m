//
//  SHStudent.m
//  Shedule
//
//  Created by Artemiy Sobolev on 03/10/13.
//  Copyright (c) 2013 Artemiy Sobolev. All rights reserved.
//

#define kName @"name"
#define kSurname @"surname"
#define kGroupNumber @"groupNumber"

#import "SHStudent.h"

@implementation SHStudent

- (BOOL)isEqual:(id)object{
    if (![object isKindOfClass:[SHStudent class]]) return NO;
    if (![self.firstName isEqualToString:[object firstName]]) return NO;
    if (![self.secondName isEqualToString:[object secondName]]) return NO;
    if (![self.groupNumber isEqualToNumber:[object groupNumber]]) return NO;
    
    return YES;
}

- (NSUInteger)hash{
    return self.firstName.hash + self.secondName.hash + self.groupNumber.hash;
}

- (NSString *)fullName{
    return [NSString stringWithFormat:@"%@ %@", self.firstName, self.secondName];
}

- (NSDictionary *)plist{
    if (!self.firstName) return nil;
    if (!self.secondName) return nil;
    if (!self.groupNumber) return nil;
    return @{kName: self.firstName, kSurname : self.secondName , kGroupNumber : self.groupNumber};
}

- (id)initWithPlist:(NSDictionary *)plist{
    if (!(self = [super init])) return nil;
    _firstName = plist[kName];
    _secondName = plist[kSurname];
    _groupNumber = plist[kGroupNumber];
    return self;
}

@end
