//
//  CarsVC.h
//  Converter
//
//  Created by Artemiy Sobolev on 26.03.15.
//  Copyright (c) 2015 Artemiy Sobolev. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface CarsVC : NSViewController
@property (nonatomic) NSMutableArray *cars;
@end
