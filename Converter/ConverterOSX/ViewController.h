//
//  ViewController.h
//  ConverterOSX
//
//  Created by Artemiy Sobolev on 26.03.15.
//  Copyright (c) 2015 Artemiy Sobolev. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface ViewController : NSViewController

@property (nonatomic) NSString *text;
@property (nonatomic) BOOL isHiden;
@property (nonatomic) NSMutableArray *currencies;
@end

