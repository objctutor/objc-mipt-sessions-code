//
//  CarsVC.m
//  Converter
//
//  Created by Artemiy Sobolev on 26.03.15.
//  Copyright (c) 2015 Artemiy Sobolev. All rights reserved.
//

#import "CarsVC.h"
#import "Car.h"

@interface CarsVC ()

@end

@implementation CarsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    Car *exampleCar = [[Car alloc] init];
    exampleCar.name = @"Lamborgini";
    self.cars = [[NSMutableArray alloc] initWithArray:@[exampleCar]];
}

@end
