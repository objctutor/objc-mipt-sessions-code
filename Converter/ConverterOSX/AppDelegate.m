//
//  AppDelegate.m
//  ConverterOSX
//
//  Created by Artemiy Sobolev on 26.03.15.
//  Copyright (c) 2015 Artemiy Sobolev. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

@end
