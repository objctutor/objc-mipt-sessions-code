//
//  ViewController.m
//  ConverterOSX
//
//  Created by Artemiy Sobolev on 26.03.15.
//  Copyright (c) 2015 Artemiy Sobolev. All rights reserved.
//

#import "ViewController.h"
#import "Currency.h"
#import "Converter.h"

@interface ViewController () <CurrencyDelegate>
@end
@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.text = @"asdf";
    self.currencies = [[NSMutableArray alloc] initWithArray:[Currency allCurrencies]];
    [self setAllCurrenciesDelegate:self];
}

- (void)setAllCurrenciesDelegate:(id<CurrencyDelegate>)delegate
{
    for (Currency *currency in self.currencies) {
        currency.delegate = delegate;
    }
}

- (void)currencyDidChange:(Currency *)currentCurrency
{
    [self setAllCurrenciesDelegate:nil];
    for (NSInteger i = 0; i < self.currencies.count; i++) {
        if (self.currencies[i] != currentCurrency) {
            CurrencyType typeAtIndex = [(Currency *)self.currencies[i] type];
            [self.currencies[i] setAmount:[[[Converter sharedInstance] convertCurrency:currentCurrency toType:typeAtIndex] amount]];
        }
    }
    [self setAllCurrenciesDelegate:self];
}

@end
