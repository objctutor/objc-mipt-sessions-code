//
//  Car.h
//  Converter
//
//  Created by Artemiy Sobolev on 26.03.15.
//  Copyright (c) 2015 Artemiy Sobolev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Car : NSObject
@property (nonatomic) NSString *name;
@end
