//
//  Converter.m
//  Converter
//
//  Created by Artemiy Sobolev on 05.03.15.
//  Copyright (c) 2015 Artemiy Sobolev. All rights reserved.
//

#import "Converter.h"

@implementation Converter

+ (Converter *)sharedInstance
{
    static dispatch_once_t onceToken;
    static Converter *result = nil;
    dispatch_once(&onceToken, ^{
        result = [[Converter alloc] init];
    });
    return result;
}

- (NSArray *)convertedValues
{
    return @[@(1), @(64.62), @(60.92)];
}

- (float)convertMultiplierType:(CurrencyType)firstType toType:(CurrencyType)secondType
{
    return [[self convertedValues][firstType] floatValue] / [[self convertedValues][secondType] floatValue];
}

- (Currency *)convertCurrency:(Currency *)currency toType:(CurrencyType)type
{
    Currency *result = [[Currency alloc] initWithType:type];
    float multiplier = [self convertMultiplierType:currency.type toType:type];
    result.amount = @(currency.amount.floatValue * multiplier);
    return result;
}

@end
