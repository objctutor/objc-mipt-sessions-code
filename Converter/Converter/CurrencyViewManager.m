//
//  CurrencyManager.m
//  Converter
//
//  Created by Artemiy Sobolev on 05.03.15.
//  Copyright (c) 2015 Artemiy Sobolev. All rights reserved.
//

#import "CurrencyViewManager.h"
#import "Currency.h"
#import "Converter.h"

@interface CurrencyViewManager () <CurrencyDelegate>

@end

@implementation CurrencyViewManager 

- (instancetype)init
{
    if (!(self = [super init])) return nil;
    NSArray *currencies = [Currency allCurrencies];
    for (Currency *currency in currencies) {
        currency.delegate = self;
    }
    _currenciesToShow = currencies;
    
    return self;
}

- (void)currencyDidChange:(Currency *)currentCurrency
{
    NSMutableArray *newCurrencies = [[NSMutableArray alloc] init];
    for (Currency *currency in self.currenciesToShow) {
        Currency *newCurrency = [[Converter sharedInstance] convertCurrency:currentCurrency toType:currency.type];
        [newCurrencies addObject:newCurrency];
    }
    
    self.currenciesToShow = newCurrencies;
    [self.delegate update];
    for (Currency *currency in self.currenciesToShow) {
        currency.delegate = self;
    }
}

@end
