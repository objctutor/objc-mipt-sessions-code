//
//  Currency.h
//  Converter
//
//  Created by Artemiy Sobolev on 05.03.15.
//  Copyright (c) 2015 Artemiy Sobolev. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, CurrencyType) {
    CurrencyTypeRUR = 0,
    CurrencyTypeEUR = 1,
    CurrencyTypeUSD = 2,
};

@class Currency;
@protocol CurrencyDelegate <NSObject>
@optional
- (void)currencyWillChange:(Currency *)currentCurrency;
- (void)currencyDidChange:(Currency *)currentCurrency;
@end

@interface Currency : NSObject
@property (nonatomic) NSString *name;
@property (nonatomic) CurrencyType type;
@property (nonatomic) NSNumber *amount;

@property (weak, nonatomic) id<CurrencyDelegate> delegate;

- (instancetype)initWithType:(CurrencyType)type;

+ (NSArray *)allCurrencies;

@end
