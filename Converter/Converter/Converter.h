//
//  Converter.h
//  Converter
//
//  Created by Artemiy Sobolev on 05.03.15.
//  Copyright (c) 2015 Artemiy Sobolev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Currency.h"

@interface Converter : NSObject

+ (Converter *)sharedInstance;

- (Currency *)convertCurrency:(Currency *)currency toType:(CurrencyType)type;

@end
