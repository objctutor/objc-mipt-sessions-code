//
//  CurrencyManager.h
//  Converter
//
//  Created by Artemiy Sobolev on 05.03.15.
//  Copyright (c) 2015 Artemiy Sobolev. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CurrencyViewManagerDelegate <NSObject>

- (void)update;

@end

@interface CurrencyViewManager : NSObject 
@property (nonatomic) NSArray *currenciesToShow;
@property (weak, nonatomic) id<CurrencyViewManagerDelegate> delegate;
@end
