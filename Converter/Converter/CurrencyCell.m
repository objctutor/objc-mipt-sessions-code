//
//  CurrencyCell.m
//  Converter
//
//  Created by Artemiy Sobolev on 05.03.15.
//  Copyright (c) 2015 Artemiy Sobolev. All rights reserved.
//

#import "CurrencyCell.h"

@implementation CurrencyCell


- (NSNumberFormatter *)formatter
{
    NSNumberFormatter *result = [[NSNumberFormatter alloc] init];
    result.numberStyle = NSNumberFormatterDecimalStyle;
    return result;
}

- (void)setCurrencyToShow:(Currency *)currencyToShow
{
    _currencyToShow = currencyToShow;
    self.currencyTypeLocalisedName.text = currencyToShow.name;
    self.amountTF.text = [[self formatter] stringFromNumber:currencyToShow.amount];
}

- (IBAction)currencyAmountChanged:(UITextField *)textField
{
    self.currencyToShow.amount = @(textField.text.floatValue);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if (selected) {
        [self.amountTF becomeFirstResponder];        
    }

}

@end
