//
//  Currency.m
//  Converter
//
//  Created by Artemiy Sobolev on 05.03.15.
//  Copyright (c) 2015 Artemiy Sobolev. All rights reserved.
//

#import "Currency.h"

@implementation Currency

- (instancetype)initWithType:(CurrencyType)type
{
    if (!(self = [super init])) return nil;
    _type = type;
    NSInteger index = [[Currency allCurrencyTypes] indexOfObject:@(type)];
    _name = [Currency allCurrencyNames][index];
    _amount = @0;
    return self;
}

- (void)setAmount:(NSNumber *)amount
{
    if ([self.delegate respondsToSelector:@selector(currencyWillChange:)]) {
        [self.delegate currencyWillChange:self];
    }
    _amount = amount;
    if ([self.delegate respondsToSelector:@selector(currencyDidChange:)]) {
        [self.delegate currencyDidChange:self];
    }
}

+ (NSArray *)allCurrencyNames
{
    return @[@"RUR", @"EUR", @"USD"];
}

+ (NSArray *)allCurrencyTypes
{
    return @[@(CurrencyTypeRUR), @(CurrencyTypeEUR), @(CurrencyTypeUSD)];
}

+ (NSArray *)allCurrencies
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    for (NSNumber * currencyTypeNumber in  [self allCurrencyTypes]) {
        [result addObject:[[Currency alloc] initWithType:currencyTypeNumber.integerValue]];
    }
    return result;
}

@end
