//
//  ViewController.m
//  MiniToDo
//
//  Created by Artemiy Sobolev on 02.04.15.
//  Copyright (c) 2015 mipt. All rights reserved.
//

#import "ViewController.h"
#import "Project.h"
#import "SingleProjectVC.h"

@implementation ViewController


- (void)setSelectionIndexes:(NSIndexSet *)selectionIndexes
{
    _selectionIndexes = selectionIndexes;
    self.spVC.project = selectionIndexes.count == 0 ? nil : [self.projectsArrayController.arrangedObjects objectAtIndex:selectionIndexes.firstIndex];
}

- (void)prepareForSegue:(NSStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"singlePersonVCSID"]) {
        self.spVC = segue.destinationController;
    }
}

- (void)setPredicateInputString:(NSString *)predicateInputString
{
    _predicateInputString = predicateInputString;
	if (predicateInputString == nil ) {
		self.projectsFilterPredicate = nil;
	}
	else {
		self.projectsFilterPredicate = [NSPredicate predicateWithFormat:@"name contains[cd] %@", predicateInputString];
	}

}

@end
