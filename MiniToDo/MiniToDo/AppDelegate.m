//
//  AppDelegate.m
//  MiniToDo
//
//  Created by Artemiy Sobolev on 02.04.15.
//  Copyright (c) 2015 mipt. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {

}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

@end
