//
//  Project.m
//  MiniToDo
//
//  Created by Artemiy Sobolev on 23.04.15.
//  Copyright (c) 2015 mipt. All rights reserved.
//

#import "Project.h"
#import "Person.h"
#import "Task.h"


@implementation Project

@dynamic name;
@dynamic textDescription;
@dynamic tasks;
@dynamic workers;

@end
