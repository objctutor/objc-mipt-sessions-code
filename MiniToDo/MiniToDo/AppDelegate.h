//
//  AppDelegate.h
//  MiniToDo
//
//  Created by Artemiy Sobolev on 02.04.15.
//  Copyright (c) 2015 mipt. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@end

