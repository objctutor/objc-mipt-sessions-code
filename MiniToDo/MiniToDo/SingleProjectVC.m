//
//  SinglePersonVC.m
//  MiniToDo
//
//  Created by Artemiy Sobolev on 02.04.15.
//  Copyright (c) 2015 mipt. All rights reserved.
//

#import "SingleProjectVC.h"
#import "CoreDataStackHelper.h"
#import "Task.h"

@interface SingleProjectVC ()

@end

@implementation SingleProjectVC


- (void)setProject:(Project *)project
{
	_project = project;
	if (project) {
		self.filterPredicate = [NSPredicate predicateWithFormat:@"project = %@", project];
	}
	else {
		self.filterPredicate = nil;
	}
}
- (IBAction)addNewTaskButtonPressed:(id)sender {
	Task *newTask = [NSEntityDescription insertNewObjectForEntityForName:@"Task" inManagedObjectContext:[[CoreDataStackHelper sharedInstance] managedObjectContext]];
	newTask.project = self.project;
}


@end
