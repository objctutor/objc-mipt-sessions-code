//
//  Person.h
//  MiniToDo
//
//  Created by Artemiy Sobolev on 23.04.15.
//  Copyright (c) 2015 mipt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Project;

@interface Person : NSManagedObject

@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * secondName;
@property (nonatomic, retain) NSSet *projects;
@end

@interface Person (CoreDataGeneratedAccessors)

- (void)addProjectsObject:(Project *)value;
- (void)removeProjectsObject:(Project *)value;
- (void)addProjects:(NSSet *)values;
- (void)removeProjects:(NSSet *)values;

@end
