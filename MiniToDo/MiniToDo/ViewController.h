//
//  ViewController.h
//  MiniToDo
//
//  Created by Artemiy Sobolev on 02.04.15.
//  Copyright (c) 2015 mipt. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class SingleProjectVC;

@interface ViewController : NSViewController

@property (nonatomic) NSIndexSet *selectionIndexes;
@property (weak) IBOutlet NSArrayController *projectsArrayController;

@property (nonatomic) NSPredicate *projectsFilterPredicate;
@property (nonatomic) NSString *predicateInputString;

@property (nonatomic, weak) SingleProjectVC *spVC;

@end

