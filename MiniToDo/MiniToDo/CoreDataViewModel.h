//
//  CoreDataViewModel.h
//  MiniToDo
//
//  Created by Artemiy Sobolev on 23.04.15.
//  Copyright (c) 2015 mipt. All rights reserved.
//

#import <Foundation/Foundation.h>
@import CoreData;

@interface CoreDataViewModel : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;

- (IBAction)saveAction:(id)sender;

@end
