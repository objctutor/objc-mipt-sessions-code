//
//  Person.m
//  MiniToDo
//
//  Created by Artemiy Sobolev on 23.04.15.
//  Copyright (c) 2015 mipt. All rights reserved.
//

#import "Person.h"
#import "Project.h"


@implementation Person

@dynamic firstName;
@dynamic secondName;
@dynamic projects;

@end
