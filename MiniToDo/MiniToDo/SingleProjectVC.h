//
//  SinglePersonVC.h
//  MiniToDo
//
//  Created by Artemiy Sobolev on 02.04.15.
//  Copyright (c) 2015 mipt. All rights reserved.
//

#import <Cocoa/Cocoa.h>
@class Project;

@interface SingleProjectVC : NSViewController
@property (nonatomic, weak) Project *project;
@property (nonatomic) NSPredicate *filterPredicate;
@end
