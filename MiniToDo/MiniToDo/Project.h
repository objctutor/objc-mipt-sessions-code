//
//  Project.h
//  MiniToDo
//
//  Created by Artemiy Sobolev on 23.04.15.
//  Copyright (c) 2015 mipt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Person, Task;

@interface Project : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * textDescription;
@property (nonatomic, retain) NSSet *tasks;
@property (nonatomic, retain) NSSet *workers;
@end

@interface Project (CoreDataGeneratedAccessors)

- (void)addTasksObject:(Task *)value;
- (void)removeTasksObject:(Task *)value;
- (void)addTasks:(NSSet *)values;
- (void)removeTasks:(NSSet *)values;

- (void)addWorkersObject:(Person *)value;
- (void)removeWorkersObject:(Person *)value;
- (void)addWorkers:(NSSet *)values;
- (void)removeWorkers:(NSSet *)values;

@end
