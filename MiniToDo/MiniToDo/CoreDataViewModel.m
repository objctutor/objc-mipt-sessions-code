//
//  CoreDataViewModel.m
//  MiniToDo
//
//  Created by Artemiy Sobolev on 23.04.15.
//  Copyright (c) 2015 mipt. All rights reserved.
//

#import "CoreDataViewModel.h"
#import "CoreDataStackHelper.h"

@implementation CoreDataViewModel

- (NSManagedObjectContext *)managedObjectContext
{
	return [[CoreDataStackHelper sharedInstance] managedObjectContext];
}

- (void)saveAction:(id)sender
{
	[[CoreDataStackHelper sharedInstance] save];
}

@end
