//
//  Task.m
//  MiniToDo
//
//  Created by Artemiy Sobolev on 23.04.15.
//  Copyright (c) 2015 mipt. All rights reserved.
//

#import "Task.h"
#import "Project.h"


@implementation Task

@dynamic textDescription;
@dynamic project;

@end
