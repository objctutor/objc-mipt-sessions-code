//
//  Notificator.m
//  NsNotificationsExample
//
//  Created by Artemiy Sobolev on 17/10/13.
//  Copyright (c) 2013 Artemiy Sobolev. All rights reserved.
//

#import "Notificator.h"

@implementation Notificator
- (void)doSomething{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"com.mipt.example" object:@[@(1), @"asdf"]];
}
@end
