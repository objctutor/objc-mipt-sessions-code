//
//  ViewController.m
//  NsNotificationsExample
//
//  Created by Artemiy Sobolev on 17/10/13.
//  Copyright (c) 2013 Artemiy Sobolev. All rights reserved.
//

#import "ViewController.h"
#import "Notificator.h"

@interface ViewController ()
@end

@implementation ViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationWasReceived:) name:@"com.mipt.example" object:nil];

}
- (void)notificationWasReceived:(NSNotification *)notification{
    NSLog(@"notification, %@", [notification object]);
}

- (IBAction)buttonPressed:(id)sender {
    Notificator *notificator = [[Notificator alloc] init];
    [notificator doSomething];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
