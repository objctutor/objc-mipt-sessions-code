//
//  Notificator.h
//  NsNotificationsExample
//
//  Created by Artemiy Sobolev on 17/10/13.
//  Copyright (c) 2013 Artemiy Sobolev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Notificator : NSObject

- (void)doSomething;

@end
