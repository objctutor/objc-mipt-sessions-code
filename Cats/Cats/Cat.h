//
//  Cat.h
//  FirstLesson
//
//  Created by artemiy on 13.09.14.
//  Copyright (c) 2014 mipt. All rights reserved.
//

#import "Pet.h"
@interface Cat : Pet;

@property(nonatomic) int numberOfColors;
@property(nonatomic) NSString *nickName;

- (instancetype)initWithNumberOfColors:(int)numberOfColors;

- (BOOL)isEqualToCat:(Cat *)cat;

@end
