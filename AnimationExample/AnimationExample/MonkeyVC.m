//
//  MonkeyVC.m
//  AnimationExample
//
//  Created by Artemiy Sobolev on 07.11.14.
//  Copyright (c) 2014 Artemiy Sobolev. All rights reserved.
//

#import "MonkeyVC.h"
#import "UIView+MotionEffects.h"

@interface MonkeyVC ()
@property (weak, nonatomic) IBOutlet UILabel *monkeyLabel;
@property (weak, nonatomic) IBOutlet UIImageView *monkeyImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewBottomContraint;
@property (weak, nonatomic) IBOutlet UIButton *monkeyButton;

@end

@implementation MonkeyVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.monkeyImage addMotionEffects:[self motionEffectsWithDelta:@(self.imageViewBottomContraint.constant)]];
    [self.monkeyLabel addMotionEffects:[self motionEffectsWithDelta:@30]];
    [self.monkeyButton addMotionEffects:[self motionEffectsWithDelta:@20]];
}

- (NSSet *)motionEffectsWithDelta:(NSNumber *)delta {
    return [NSSet setWithArray:@[[self motionEffectForKeyPath:@"center.x" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis delta:delta], [self motionEffectForKeyPath:@"center.y" type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis delta:delta]]];
}

- (UIInterpolatingMotionEffect *)motionEffectForKeyPath:(NSString *)keyPath type:(UIInterpolatingMotionEffectType)type delta:(NSNumber *)delta {
    UIInterpolatingMotionEffect *result = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:keyPath type:type];
    result.minimumRelativeValue = @(delta.floatValue * -1);
    result.maximumRelativeValue = delta;
    return result;
}

@end
