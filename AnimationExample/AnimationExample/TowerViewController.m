//
//  TowerViewController.m
//  AnimationExample
//
//  Created by Artemiy Sobolev on 08.11.14.
//  Copyright (c) 2014 Artemiy Sobolev. All rights reserved.
//

#import "TowerViewController.h"
@import CoreMotion;

@interface TowerViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *towerImage;
@property (nonatomic) CMMotionManager *manager;
@property (nonatomic) NSOperationQueue *backGroundQueue;
@end

@implementation TowerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.backGroundQueue = [[NSOperationQueue alloc] init];
    self.backGroundQueue.maxConcurrentOperationCount = 1;
    self.manager = [[CMMotionManager alloc] init];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (!self.manager.deviceMotionAvailable) {
        return;
    }
    
    [self.manager setDeviceMotionUpdateInterval:0.01];
    __weak __typeof(self) weakSelf = self;
    [self.manager startDeviceMotionUpdatesToQueue:self.backGroundQueue withHandler:^(CMDeviceMotion *motion, NSError *error) {
        __typeof(weakSelf) strongSelf = weakSelf;
        double rotation = atan2(motion.gravity.x, motion.gravity.y) - M_PI;
        dispatch_async(dispatch_get_main_queue(), ^{
            strongSelf.towerImage.transform = CGAffineTransformMakeRotation(rotation);
            if (motion.userAcceleration.x < -2) {
                [self dismissViewControllerAnimated:YES completion:nil];
            }
            else if (motion.userAcceleration.x > 2) {
                [self performSegueWithIdentifier:@"toBasicAnimationsVC" sender:self];
            }
        });
    }];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.manager stopDeviceMotionUpdates];
}


@end
