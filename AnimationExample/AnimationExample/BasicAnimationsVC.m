//
//  BasicAnimationsVC.m
//  AnimationExample
//
//  Created by Artemiy Sobolev on 08.11.14.
//  Copyright (c) 2014 Artemiy Sobolev. All rights reserved.
//

#import "BasicAnimationsVC.h"
const CGFloat BasicAnimationsBoxSize = 50;

@interface BasicAnimationsVC ()
@property (weak, nonatomic) IBOutlet UIView *greenBox;
@property (weak, nonatomic) IBOutlet UISlider *springVelocitySlider;
@property (weak, nonatomic) IBOutlet UISlider *springDumpingSlider;

@end

@implementation BasicAnimationsVC

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self moveBoxToStartAnimationPoint];
}

- (IBAction)tapGestureRecogniser:(UIGestureRecognizer *)gestureRecognizer {
    [UIView animateWithDuration:1 delay:0 usingSpringWithDamping:self.springDumpingSlider.value initialSpringVelocity:self.springVelocitySlider.value options:UIViewAnimationOptionCurveLinear animations:^{
        [self moveBoxToCenter];
    } completion:^(BOOL finished) {
        if (finished) {
            [self moveBoxToStartAnimationPoint];            
        }

    }];
}

- (void)moveBoxToStartAnimationPoint {
    self.greenBox.frame = CGRectMake(self.view.frame.size.width / 2 - BasicAnimationsBoxSize / 2, self.view.frame.size.height, BasicAnimationsBoxSize, BasicAnimationsBoxSize);
}

- (void)moveBoxToCenter {
    self.greenBox.frame = CGRectMake(self.view.frame.size.width / 2 - BasicAnimationsBoxSize / 2, self.view.frame.size.height / 2 - BasicAnimationsBoxSize / 2, BasicAnimationsBoxSize, BasicAnimationsBoxSize);
}

@end
