//
//  ViewController.m
//  AnimationExample
//
//  Created by Artemiy Sobolev on 14/11/13.
//  Copyright (c) 2013 Artemiy Sobolev. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIView *viewToAnimate;
@property (nonatomic) NSTimer *timer;
@end

@implementation ViewController


- (IBAction)animation1:(id)sender {
    [UIView animateWithDuration:0.2 animations:^{
        self.viewToAnimate.transform = CGAffineTransformMakeScale(2, 1);
    } completion:^(BOOL finished){
        [UIView animateWithDuration:0.3 animations:^{
            self.viewToAnimate.transform = CGAffineTransformMakeScale(1, 1);
        }];
    }];
}

- (void)rotate{
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        self.viewToAnimate.transform = CGAffineTransformRotate(self.viewToAnimate.transform, M_PI/2);
    } completion:^(BOOL finished){
        [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
            self.viewToAnimate.transform = CGAffineTransformRotate(self.viewToAnimate.transform, M_PI/2);
        } completion:^(BOOL finished) {
            
        }];
    }];
}

- (IBAction)animation2:(id)sender {
    [self rotate];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:0.6 target:self selector:@selector(rotate) userInfo:nil repeats:YES];
}

- (IBAction)stopInfiniteRotation:(id)sender {
    [self.timer invalidate];
}

- (IBAction)animation3:(id)sender {
    [UIView animateWithDuration:0.2 animations:^{
        CGRect rect = self.viewToAnimate.frame;
        rect.origin.y += 10;
        [self.viewToAnimate setFrame:rect];
    }];
    
}

- (IBAction)animtaion4:(id)sender {
    [UIView animateWithDuration:0.2 animations:^{
        self.viewToAnimate.backgroundColor = [self randomColor];
    }];
}

- (UIColor *)randomColor{
    CGFloat hue = ( arc4random() % 256 / 256.0 );
    CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;
    CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;
    return [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
    
}

- (void)multiStepAnimationExample{
    NSMutableArray* animationBlocks = [NSMutableArray new];
    
    typedef void(^animationBlock)(BOOL);
    
    // getNextAnimation
    // removes the first block in the queue and returns it
    animationBlock (^getNextAnimation)() = ^{
        animationBlock block = (animationBlock)[animationBlocks firstObject];
        if (block){
            [animationBlocks removeObjectAtIndex:0];
            return block;
        }else{
            return ^(BOOL finished){};
        }
    };
    
    //add a block to our queue
    [animationBlocks addObject:^(BOOL finished){
        [UIView animateWithDuration:1.0 animations:^{
            //...animation code...
        } completion: getNextAnimation()];
    }];
    
    //add a block to our queue
    [animationBlocks addObject:^(BOOL finished){
        [UIView animateWithDuration:1.0 animations:^{
            //...animation code...
        } completion: getNextAnimation()];
    }];
    
    //add a block to our queue
    [animationBlocks addObject:^(BOOL finished){;
        NSLog(@"Multi-step Animation Complete!");
    }];
    
    // execute the first block in the queue
    getNextAnimation()(YES);
}


@end
