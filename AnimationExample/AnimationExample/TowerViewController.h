//
//  TowerViewController.h
//  AnimationExample
//
//  Created by Artemiy Sobolev on 08.11.14.
//  Copyright (c) 2014 Artemiy Sobolev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PortraitViewController.h"

@interface TowerViewController : PortraitViewController

@end
