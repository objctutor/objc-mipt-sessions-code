//
//  UIView+MotionEffects.h
//  AnimationExample
//
//  Created by Artemiy Sobolev on 07.11.14.
//  Copyright (c) 2014 Artemiy Sobolev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (MotionEffects)
- (void)addMotionEffects:(NSSet *)objects;
@end
