//
//  PortraitViewController.m
//  AnimationExample
//
//  Created by Artemiy Sobolev on 08.11.14.
//  Copyright (c) 2014 Artemiy Sobolev. All rights reserved.
//

#import "PortraitViewController.h"

@implementation PortraitViewController

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIDeviceOrientationPortrait;
}

@end
