//
//  UIView+MotionEffects.m
//  AnimationExample
//
//  Created by Artemiy Sobolev on 07.11.14.
//  Copyright (c) 2014 Artemiy Sobolev. All rights reserved.
//

#import "UIView+MotionEffects.h"

@implementation UIView (MotionEffects)
- (void)addMotionEffects:(NSSet *)objects{
    for (UIMotionEffect *effect in objects) {
        [self addMotionEffect:effect];
    }
}
@end
