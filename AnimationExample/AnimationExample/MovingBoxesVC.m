//
//  MovingBoxesVC.m
//  AnimationExample
//
//  Created by Artemiy Sobolev on 08.11.14.
//  Copyright (c) 2014 Artemiy Sobolev. All rights reserved.
//

#import "MovingBoxesVC.h"

@interface MovingBoxesVC ()
@property (weak, nonatomic) IBOutlet UIView *blueBox;
@end

@implementation MovingBoxesVC

- (IBAction)panHappened:(UIPanGestureRecognizer *)pgr {
    CGPoint translation = [pgr translationInView:self.view];
    self.blueBox.center = CGPointMake(self.blueBox.center.x + translation.x, self.blueBox.center.y + translation.y);
    [pgr setTranslation:CGPointZero inView:self.view];
}

- (IBAction)rotationGestureRecognizer:(UIRotationGestureRecognizer *)rgr {
    self.blueBox.transform = CGAffineTransformRotate(self.blueBox.transform, rgr.rotation);
    rgr.rotation = 0;
}

- (IBAction)pinchGestureRecognizer:(UIPinchGestureRecognizer *)pgr {
    self.blueBox.transform = CGAffineTransformScale(self.blueBox.transform, pgr.scale, pgr.scale);
    pgr.scale = 1;
}

@end
