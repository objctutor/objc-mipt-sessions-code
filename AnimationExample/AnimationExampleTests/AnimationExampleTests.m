//
//  AnimationExampleTests.m
//  AnimationExampleTests
//
//  Created by Artemiy Sobolev on 14/11/13.
//  Copyright (c) 2013 Artemiy Sobolev. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface AnimationExampleTests : XCTestCase

@end

@implementation AnimationExampleTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
