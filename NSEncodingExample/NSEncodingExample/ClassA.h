//
//  ClassA.h
//  NSEncodingExample
//
//  Created by Artemiy Sobolev on 06/11/13.
//  Copyright (c) 2013 Artemiy Sobolev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ClassB.h"

@interface ClassA : NSObject <NSCoding>
@property (nonatomic, strong) ClassB *classB;

@end
