//
//  AppDelegate.m
//  NSEncodingExample
//
//  Created by Artemiy Sobolev on 06/11/13.
//  Copyright (c) 2013 Artemiy Sobolev. All rights reserved.
//
#define path @"asdf"

#import "AppDelegate.h"
#import "ClassB.h"
#import "ClassA.h"


@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification{
    ClassA *classAInstance = [[ClassA alloc] init];

    ClassB *classBInstance = [[ClassB alloc] init];
    classBInstance.name = @"the super name";
    classBInstance.someNumber = @(0.3);
    
    
    classAInstance.classB = classBInstance;
    
    [NSKeyedArchiver archiveRootObject:classAInstance toFile:path];
    
    
    
    ClassA *aInstanceFromFile = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
    
    NSLog(@"the properties are \"%@\" and \"%@\"", aInstanceFromFile.classB.name, aInstanceFromFile.classB.someNumber);
    
    
    
}

@end
