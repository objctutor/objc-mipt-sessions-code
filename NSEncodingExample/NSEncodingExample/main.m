//
//  main.m
//  NSEncodingExample
//
//  Created by Artemiy Sobolev on 06/11/13.
//  Copyright (c) 2013 Artemiy Sobolev. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
