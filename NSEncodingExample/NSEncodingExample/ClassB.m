//
//  ClassB.m
//  NSEncodingExample
//
//  Created by Artemiy Sobolev on 06/11/13.
//  Copyright (c) 2013 Artemiy Sobolev. All rights reserved.
//

#define kName @"name"
#define kSomeNumber @"someNumber"

#import "ClassB.h"

@implementation ClassB

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    if (!self) return nil;
    
    _name = [aDecoder decodeObjectForKey:kName];
    _someNumber = [aDecoder decodeObjectForKey:kSomeNumber];
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.name forKey:kName];
    [aCoder encodeObject:self.someNumber forKey:kSomeNumber];
}

@end
