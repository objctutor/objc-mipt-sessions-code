//
//  ClassB.h
//  NSEncodingExample
//
//  Created by Artemiy Sobolev on 06/11/13.
//  Copyright (c) 2013 Artemiy Sobolev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ClassB : NSObject <NSCoding>

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSNumber *someNumber;

@end
