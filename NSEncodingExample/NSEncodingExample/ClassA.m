//
//  ClassA.m
//  NSEncodingExample
//
//  Created by Artemiy Sobolev on 06/11/13.
//  Copyright (c) 2013 Artemiy Sobolev. All rights reserved.
//

#define kClassBEntity @"classBEntity"
#import "ClassA.h"

@implementation ClassA

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    if (!self) return nil;
    _classB = [aDecoder decodeObjectForKey:kClassBEntity];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.classB forKey:kClassBEntity];
}

@end
