//
//  AppDelegate.h
//  NSEncodingExample
//
//  Created by Artemiy Sobolev on 06/11/13.
//  Copyright (c) 2013 Artemiy Sobolev. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
