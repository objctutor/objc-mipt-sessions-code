//
//  StudentCell.h
//  TableViews
//
//  Created by Artemiy Sobolev on 29.11.14.
//  Copyright (c) 2014 avriy. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Developer;
@interface StudentCell : UITableViewCell
@property (weak, nonatomic) Developer *relatedStudent;

@property (weak, nonatomic) IBOutlet UIImageView *studentImageView;
@property (weak, nonatomic) IBOutlet UILabel *studentNameLabel;

@end
