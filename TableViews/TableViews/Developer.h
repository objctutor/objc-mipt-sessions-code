//
//  Student.h
//  TableViews
//
//  Created by nastia on 15/11/14.
//  Copyright (c) 2014 avriy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Developer : NSObject <NSCoding>
@property (nonatomic) NSString *name;
@property (nonatomic) UIImage *studentImage;
@end
