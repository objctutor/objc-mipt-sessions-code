//
//  DevelopersStorage.m
//  TableViews
//
//  Created by Artemiy Sobolev on 06.12.14.
//  Copyright (c) 2014 avriy. All rights reserved.
//

#import "DevelopersStorage.h"
#import "Developer.h"

NSString * const kStudentsFile = @"students.plist";
NSString * const kIsIniCloud = @"is in iCloud key";

@interface DevelopersStorage()
@property (readwrite, nonatomic) NSArray *developers;
@property (nonatomic) NSURL *iCloudDocumentsDirectory;
@end

@implementation DevelopersStorage
@synthesize developers = _developers;

- (void)setDevelopers:(NSArray *)students {
    _developers = students;
    [self saveStudentsAsync];
}

- (void)addNewDeveloper:(Developer *)developer {
    self.developers = [self.developers arrayByAddingObject:developer];
}

- (void)changeDeveloperAtIndex:(NSInteger)index to:(Developer *)newDeveloper {
    NSMutableArray *developersCopy = [NSMutableArray arrayWithArray:self.developers];
    developersCopy[index] = newDeveloper;
    self.developers = developersCopy;
}

- (void)removeDeveloperAtIndex:(NSInteger)index{
    NSMutableArray *developersCopy = [NSMutableArray arrayWithArray:self.developers];
    [developersCopy removeObjectAtIndex:index];
    self.developers = developersCopy;
}

- (NSURL *)iCloudDocumentsDirectory{
    if (!_iCloudDocumentsDirectory) {
        _iCloudDocumentsDirectory = [[[NSFileManager defaultManager] URLForUbiquityContainerIdentifier:nil] URLByAppendingPathComponent:@"Documents"];
    }
    return _iCloudDocumentsDirectory;
}

- (NSArray *)developers {
    if (!_developers) {
        _developers = [NSKeyedUnarchiver unarchiveObjectWithFile:[[self actualFileURL] path]];
        if (!_developers) {
            _developers = @[];
        }
    }
    return _developers;
}

- (NSURL *)actualFileURL {
    return 1 ? [self iCloudFileURL] : [self localFileURL];
}

- (void)saveStudentsAsync {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSString *filePath = [[self actualFileURL] path];
        assert([NSKeyedArchiver archiveRootObject:self.developers toFile:filePath]);
        
//        NSError *error;
//        [[NSFileManager defaultManager] setUbiquitous:YES itemAtURL:[self localFileURL] destinationURL:[self iCloudFileURL] error:&error];
//        if (error) {
//            
//        }
    });
}

- (NSString *)localFilePath:(NSString *)fileName {
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:fileName];
}

- (NSURL *)localFileURL {
    NSURL *result = [[NSURL alloc] initFileURLWithPath:[self localFilePath:kStudentsFile]];
    return result;
}

- (NSURL *)iCloudFileURL {
    return [self.iCloudDocumentsDirectory URLByAppendingPathComponent:kStudentsFile];
}


@end
