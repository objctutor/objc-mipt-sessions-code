//
//  StudentInfoVC.m
//  TableViews
//
//  Created by nastia on 15/11/14.
//  Copyright (c) 2014 avriy. All rights reserved.
//

#import "StudentInfoVC.h"
#import "Developer.h"

@interface StudentInfoVC ()
@property (weak, nonatomic) IBOutlet UITextField *studentNameTextField;
@property (weak, nonatomic) IBOutlet UIImageView *studentImageView;
@property (nonatomic) Developer *editedStudent;
@property (nonatomic) UIImagePickerController *imagePickerC;
@end

@implementation StudentInfoVC
- (void)viewDidLoad {
    [super viewDidLoad];
    self.studentNameTextField.text = self.student.name;
    if (self.student.studentImage) {
        self.studentImageView.image = self.student.studentImage;
    }
    self.studentImageView.userInteractionEnabled = YES;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [self imagePickerC];
    });
}

- (UIImagePickerController *)imagePickerC {
    if (!_imagePickerC) {
        _imagePickerC = [[UIImagePickerController alloc] init];
        _imagePickerC.delegate = self;
        _imagePickerC.allowsEditing = YES;
        _imagePickerC.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    return _imagePickerC;
}

- (IBAction)tapped:(id)sender {
    [self presentViewController:self.imagePickerC animated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.studentImageView.image = chosenImage;
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (IBAction)saveButtonPressed:(id)sender {
    NSString *studentName = self.studentNameTextField.text;
    if (studentName) {
        Developer *newStudent = [[Developer alloc] init];
        newStudent.name = studentName;
        newStudent.studentImage = self.studentImageView.image;

        if (!self.student) {
            [self.delegate addNewStudent:newStudent];
        }
        else {
            [self.delegate changeStudent:self.student toNewStudent:newStudent];
        }
    }
    [self performSegueWithIdentifier:@"BackToTableVCSID" sender:self];
}

@end
