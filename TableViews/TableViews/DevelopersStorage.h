//
//  DevelopersStorage.h
//  TableViews
//
//  Created by Artemiy Sobolev on 06.12.14.
//  Copyright (c) 2014 avriy. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Developer;
@interface DevelopersStorage : NSObject
@property (readonly, nonatomic) NSArray *developers;

- (void)addNewDeveloper:(Developer *)developer;
- (void)removeDeveloperAtIndex:(NSInteger)index;

- (void)changeDeveloperAtIndex:(NSInteger)index to:(Developer *)newDeveloper;

@end
