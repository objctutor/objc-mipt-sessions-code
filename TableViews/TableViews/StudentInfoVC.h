//
//  StudentInfoVC.h
//  TableViews
//
//  Created by nastia on 15/11/14.
//  Copyright (c) 2014 avriy. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Developer;

@protocol StudentInfoChangeStudentProtocol
- (void)addNewStudent:(Developer *)newStudent;
- (void)changeStudent:(Developer *)oldStudent toNewStudent:(Developer *)newStudent;
@end

@interface StudentInfoVC : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (nonatomic, weak) Developer *student;
@property (nonatomic, weak) id<StudentInfoChangeStudentProtocol> delegate;

@end
