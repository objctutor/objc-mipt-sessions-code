//
//  StudentsTVC.m
//  TableViews
//
//  Created by nastia on 15/11/14.
//  Copyright (c) 2014 avriy. All rights reserved.
//

#import "StudentsTVC.h"
#import "Developer.h"
#import "StudentInfoVC.h"
#import "StudentCell.h"
#import "DevelopersStorage.h"


@interface StudentsTVC () <StudentInfoChangeStudentProtocol>
@property (nonatomic) DevelopersStorage *storage;
@end

@implementation StudentsTVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.storage = [[DevelopersStorage alloc] init];
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.storage.developers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"StudentCellRID" forIndexPath:indexPath];
    [(StudentCell *)cell setRelatedStudent:self.storage.developers[indexPath.row]];
    return cell;
}

- (void)addNewStudent:(Developer *)newStudent {
    [self.storage addNewDeveloper:newStudent];
}

- (void)changeStudent:(Developer *)oldStudent toNewStudent:(Developer *)newStudent {
    NSUInteger oldStudentIndex = [self.storage.developers indexOfObject:oldStudent];
    [self.storage changeDeveloperAtIndex:oldStudentIndex to:newStudent];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.storage removeDeveloperAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

#pragma mark - Navigation
- (IBAction)backToTableVC:(UIStoryboardSegue *)unwindSegue {
    [self.tableView reloadData];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    StudentInfoVC *dvc = segue.destinationViewController;
    dvc.delegate = self;
    if ([sender isKindOfClass:[UITableViewCell class]]) {
        NSIndexPath *indexPathForSelectedStudent = [self.tableView indexPathForCell:(UITableViewCell *)sender];
        dvc.student = self.storage.developers[indexPathForSelectedStudent.row];
    }
}


@end
