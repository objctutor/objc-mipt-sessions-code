//
//  AppDelegate.h
//  TableViews
//
//  Created by nastia on 15/11/14.
//  Copyright (c) 2014 avriy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

