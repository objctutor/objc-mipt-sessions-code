//
//  Student.m
//  TableViews
//
//  Created by nastia on 15/11/14.
//  Copyright (c) 2014 avriy. All rights reserved.
//

#import "Developer.h"

NSString * const kStudentName = @"name";
NSString * const kStudentImageData = @"image";

@implementation Developer

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (!(self = [self init]))  return nil;
    _name = [aDecoder decodeObjectForKey:kStudentName];
    if (!_name) {
        return nil;
    }
    NSData *data = [aDecoder decodeObjectForKey:kStudentImageData];
    if (data) {
        _studentImage = [UIImage imageWithData:data];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.name forKey:kStudentName];
    NSData *imageData = UIImagePNGRepresentation( self.studentImage);
    [aCoder encodeObject:imageData forKey:kStudentImageData];
}

@end
