//
//  StudentCell.m
//  TableViews
//
//  Created by Artemiy Sobolev on 29.11.14.
//  Copyright (c) 2014 avriy. All rights reserved.
//

#import "StudentCell.h"
#import "Developer.h"

@implementation StudentCell

- (void)setRelatedStudent:(Developer *)relatedStudent {
    _relatedStudent = relatedStudent;
    self.studentNameLabel.text = relatedStudent.name;
    self.studentImageView.image = relatedStudent.studentImage ? relatedStudent.studentImage : [UIImage imageNamed:@"boy"];
}

@end
