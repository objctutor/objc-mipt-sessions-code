//
//  DelegateMaster.m
//  DelegateExample
//
//  Created by Artemiy Sobolev on 17/10/13.
//  Copyright (c) 2013 Artemiy Sobolev. All rights reserved.
//

#import "DelegateMaster.h"
@interface DelegateMaster()
@property (nonatomic, strong) DelegateSlave *delegateSlave;
@end

@implementation DelegateMaster
- (void)doSMThingFromIBAction{
    self.delegateSlave = [[DelegateSlave alloc] init];
//    self.delegateSlave.delegate = self;
    [self.delegateSlave doSomething];
}

- (void)updateMeOneMoreTime{
    [self.delegateSlave doSomething];
}
- (void)imDone{
    NSLog(@"slave tells he is done!");
}

@end
