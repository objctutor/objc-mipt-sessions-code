//
//  DelegateSlave.h
//  DelegateExample
//
//  Created by Artemiy Sobolev on 17/10/13.
//  Copyright (c) 2013 Artemiy Sobolev. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol DelegateSlaveProtocol
- (void)updateMeOneMoreTime;
- (void)imDone;
@end

@interface DelegateSlave : NSObject


@property (weak) id delegate;
- (void)doSomething;


@end
