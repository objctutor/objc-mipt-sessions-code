//
//  ViewController.m
//  DelegateExample
//
//  Created by Artemiy Sobolev on 17/10/13.
//  Copyright (c) 2013 Artemiy Sobolev. All rights reserved.
//

#import "ViewController.h"
#import "DelegateMaster.h"

@interface ViewController ()
@property (nonatomic, strong) DelegateMaster *master;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}
- (IBAction)makeIt:(id)sender {
    self.master = [[DelegateMaster alloc] init];
    [self.master doSMThingFromIBAction];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
