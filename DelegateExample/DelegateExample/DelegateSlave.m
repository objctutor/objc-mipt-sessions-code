//
//  DelegateSlave.m
//  DelegateExample
//
//  Created by Artemiy Sobolev on 17/10/13.
//  Copyright (c) 2013 Artemiy Sobolev. All rights reserved.
//

#import "DelegateSlave.h"
@interface DelegateSlave()
@property (nonatomic) NSInteger counter;
@end

@implementation DelegateSlave
- (void)doSomething{
    NSLog(@"smt");
    self.counter++;
    if (self.counter == 1) {
        [_delegate updateMeOneMoreTime];
    }
    else
        [_delegate imDone];
}
@end

