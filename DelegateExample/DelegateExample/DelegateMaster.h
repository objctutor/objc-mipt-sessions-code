//
//  DelegateMaster.h
//  DelegateExample
//
//  Created by Artemiy Sobolev on 17/10/13.
//  Copyright (c) 2013 Artemiy Sobolev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DelegateSlave.h"

@interface DelegateMaster : NSObject <DelegateSlaveProtocol>
- (void)doSMThingFromIBAction;

@end
